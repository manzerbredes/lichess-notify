# Lichess Games Notifications

### How to use it ?
 1. Install [Berserk](https://github.com/rhgrant10/berserk) with `pip install berserk`
 2. Generate an access token on [Lichess.org](https://lichess.org)
 3. Set the value of **ACCESS_TOKEN** variable in *notify.py* to the one use in step 2
 4. Now each time you want to check for notification simply run **./notify.py** (use crontab maybe)
### Dependencies
- `notify-send` This command should be available!
- `berserk` The lichess python API
