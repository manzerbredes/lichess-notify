#!/usr/bin/env python

import berserk, subprocess, pickle
from datetime import datetime
from os import path,remove

# Change ACCESS TOKEN according to your need
ACCESS_TOKEN=""
NOTIFY_DURATION=15*60 # Notification duration in seconds
CACHE_FILE="/tmp/lichess_notify_cache" # Change this according to your needs
CACHE_EXPIRE=300 # Change cache expiration in minutes

# Notify using notify-send
def notify_send(summary, message):
    subprocess.Popen(['notify-send', '-u', 'critical','-t', str(NOTIFY_DURATION*1000), summary, message])
    return


# Check if notify already done
def notify_done(key):
    if not(path.exists(CACHE_FILE)):
        return(False)
    else:
        data=dict()
        with open(CACHE_FILE, 'rb') as f:
            data=pickle.load(f)
        if datetime.timestamp(datetime.now()) - data[key] >= CACHE_EXPIRE*60:
            remove(CACHE_FILE)
            return(False)
        else:
            return(key in data)

# Save notify key in cache
def add_key(key):
    if not(path.exists(CACHE_FILE)):
        with open(CACHE_FILE, 'wb') as f:
            pickle.dump({key:datetime.timestamp(datetime.now())},f)
    else:
        data=list()
        with open(CACHE_FILE, 'rb') as f:
            data=pickle.load(f)
        if not(key in data["keys"]):
            data[key]=datetime.timestamp(datetime.now())
        with open(CACHE_FILE, 'wb') as f:
            pickle.dump(data,f)

# Fetch data and notify
session = berserk.TokenSession(ACCESS_TOKEN)
client = berserk.Client(session=session)
data=client.games.get_ongoing()
for game in data:
    opponent=game["opponent"]["username"]
    lastMove=game["lastMove"]
    key=opponent+lastMove
    if game["isMyTurn"]:
        if not(notify_done(key)):
            notify_send("Lichess.org ("+opponent+")","It is your turn !\n Your opponent played "+lastMove)
            add_key(key)

